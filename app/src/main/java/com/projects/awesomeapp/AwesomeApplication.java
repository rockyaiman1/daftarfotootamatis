package com.projects.awesomeapp;

import android.app.Application;

public class AwesomeApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        GlobalApp.getNewInstance().initDatabase(getApplicationContext());
    }
}