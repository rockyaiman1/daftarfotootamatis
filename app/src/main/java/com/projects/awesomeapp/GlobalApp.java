package com.projects.awesomeapp;

import android.content.Context;

import androidx.room.Room;

import com.projects.awesomeapp.connections.databases.InterfacePhotoListDatabase;
import com.projects.awesomeapp.connections.databases.PhotoDatabase;
import com.projects.awesomeapp.connections.databases.PhotoDatabaseDefinition;

public class GlobalApp {
    private static final GlobalApp newInstance = new GlobalApp();

    private PhotoDatabaseDefinition databaseDefinition;

    public static GlobalApp getNewInstance() {
        return newInstance;
    }

    private GlobalApp(){}

    public InterfacePhotoListDatabase getPhotoDatabase(Context context){
        return new PhotoDatabase(context);
    }

    public void initDatabase(Context context){
        databaseDefinition = Room.databaseBuilder(context, PhotoDatabaseDefinition.class, context.getString(R.string.app_name)).build();
    }

    public PhotoDatabaseDefinition getDatabaseDefinition(){
        return databaseDefinition;
    }
}
