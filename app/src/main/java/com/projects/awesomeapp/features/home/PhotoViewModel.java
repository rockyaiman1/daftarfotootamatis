package com.projects.awesomeapp.features.home;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.projects.awesomeapp.GlobalApp;
import com.projects.awesomeapp.connections.databases.InterfacePhotoListDatabase;
import com.projects.awesomeapp.connections.responses.DatabaseResponseErrorListener;
import com.projects.awesomeapp.connections.responses.DatabaseResponseSuccessListener;
import com.projects.awesomeapp.helpers.GlobalVariable;
import com.projects.awesomeapp.models.PhotoModel;
import com.projects.awesomeapp.connections.networks.UrlConverterRequest;
import com.projects.awesomeapp.connections.networks.UrlPath;
import com.projects.awesomeapp.connections.requests.GetPhotoListRequest;
import com.projects.awesomeapp.connections.responses.GetPhotoListResponse;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PhotoViewModel extends AndroidViewModel {

    protected InterfacePhotoListDatabase database;
    private List<PhotoModel> photoModels = new ArrayList<>();
    private final RequestQueue requestQueue;
    private final MutableLiveData<List<PhotoModel>> photoLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> isHasMorePage = new MutableLiveData<>();

    public PhotoViewModel(@NonNull Application application) {
        super(application);
        requestQueue = Volley.newRequestQueue(application);
        database = GlobalApp.getNewInstance().getPhotoDatabase(application);
    }

    public void getPhotoList(GetPhotoListRequest request){
        UrlConverterRequest getRequest = new UrlConverterRequest(Request.Method.GET, UrlPath.getListPhoto(), request,
                response -> {
                    GetPhotoListResponse getPhotoListResponse = new GetPhotoListResponse(response);
                    if (getPhotoListResponse.getPhotoModelList() != null){
                        photoModels.addAll(getPhotoListResponse.getPhotoModelList());
                        photoLiveData.postValue(photoModels);
                        isHasMorePage.postValue(getPhotoListResponse.getPhotoModelList().size() >= GlobalVariable.PAGE_SIZE);
                    } else {
                        isHasMorePage.postValue(false);
                    }
                    Log.v("APIresponses", getPhotoListResponse.getPhotoModelList().toString());
                },
                error -> Log.v("APIError", error.toString()));
        requestQueue.add(getRequest);
    }

    public void insertPhotoListToDatabase(List<PhotoModel> photoModels){
        for (PhotoModel model : photoModels){
            database.insertPhoto(
                    model,
                    new DatabaseResponseSuccessListener<Long>() {
                        @Override
                        public void onSuccessResponse(Long results) {

                        }
                    },
                    new DatabaseResponseErrorListener() {
                        @Override
                        public void onErrorResponseListener() {

                        }
                    }
            );
        }
    }

    public void getPhotoListFromDatabase(GetPhotoListRequest request){
        database.getPhotoList(
                request,
                new DatabaseResponseSuccessListener<List<PhotoModel>>() {
                    @Override
                    public void onSuccessResponse(List<PhotoModel> results) {
                        photoLiveData.postValue(results);
                    }
                },
                new DatabaseResponseErrorListener() {
                    @Override
                    public void onErrorResponseListener() {

                    }
                }
        );
    }
}
