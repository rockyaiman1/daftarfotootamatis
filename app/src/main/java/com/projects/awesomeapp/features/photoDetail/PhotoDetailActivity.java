package com.projects.awesomeapp.features.photoDetail;

import android.content.Intent;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.projects.awesomeapp.R;
import com.projects.awesomeapp.databinding.ActivityPhotoDetailBinding;
import com.projects.awesomeapp.features.BaseActivity;
import com.projects.awesomeapp.helpers.GlobalVariable;
import com.projects.awesomeapp.models.PhotoModel;

public class PhotoDetailActivity extends BaseActivity<ActivityPhotoDetailBinding> {

    private PhotoModel photoModel;

    @Override
    protected int attachLayout() {
        return R.layout.activity_photo_detail;
    }

    @Override
    protected void initData() {
        super.initData();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        photoModel = (PhotoModel) bundle.getSerializable(GlobalVariable.KEY_PHOTO_DETAIL);
    }

    @Override
    protected void initLayout() {
        super.initLayout();

        Glide.with(this).asBitmap().load(photoModel.getOriginal()).into(binding.fieldPhotoDetail);
        binding.setPhotoModel(photoModel);
    }

    @Override
    protected void initAction() {
        super.initAction();

        binding.icBack.setOnClickListener(onClick -> onBackPressed());
    }
}
