package com.projects.awesomeapp.models;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SrcModel {

    @SerializedName("original")
    private String original;

    @SerializedName("medium")
    private String medium;

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public SrcModel(JSONObject jsonObject){
        try {
            if (jsonObject.has("original")){
                setOriginal(jsonObject.getString("original"));
            }
            if (jsonObject.has("medium")){
                setMedium(jsonObject.getString("medium"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
