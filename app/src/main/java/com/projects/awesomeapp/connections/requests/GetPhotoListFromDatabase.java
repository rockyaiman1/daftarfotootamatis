package com.projects.awesomeapp.connections.requests;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter

public class GetPhotoListFromDatabase {
    private int pageNo;
    private int pageSize;
}
