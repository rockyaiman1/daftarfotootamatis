package com.projects.awesomeapp.connections.requests;

import com.projects.awesomeapp.helpers.GlobalVariable;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class GetPhotoListRequest extends BaseRequest {
    private int page;
    private int perPage;

    public int getPage() {
        return page;
    }

    public int getPerPage() {
        return perPage;
    }

    @Override
    protected void populateParameters() {
        parameters.put(GlobalVariable.KEY_URL_PAGE, getPage());
        parameters.put(GlobalVariable.KEY_URL_PER_PAGE, getPerPage());
    }
}
