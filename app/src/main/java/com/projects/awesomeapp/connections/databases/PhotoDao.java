package com.projects.awesomeapp.connections.databases;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.projects.awesomeapp.models.PhotoModel;

import java.util.List;

@Dao
public interface PhotoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert (PhotoModel photoModel);

    @Query("SELECT * FROM PhotoModel ORDER BY id ASC LIMIT :pageNo, :pageSize")
    List<PhotoModel> getMovieList (Integer pageNo, Integer pageSize);
}
