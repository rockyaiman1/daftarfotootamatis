package com.projects.awesomeapp.connections.databases;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.projects.awesomeapp.BuildConfig;
import com.projects.awesomeapp.models.PhotoModel;

@Database(
        entities = {PhotoModel.class}, version = BuildConfig.DATABASE_VERSION_CODE, exportSchema = false
)

public abstract class PhotoDatabaseDefinition extends RoomDatabase {
    public abstract PhotoDao photoDao();
}
