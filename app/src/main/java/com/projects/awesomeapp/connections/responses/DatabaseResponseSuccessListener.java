package com.projects.awesomeapp.connections.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class DatabaseResponseSuccessListener<R> implements Runnable {
    private R result;

    public R getResult() {
        return result;
    }

    public void setResult(R result) {
        this.result = result;
    }

    @Override
    public void run() {
        onSuccessResponse(getResult());
    }

    public abstract void onSuccessResponse(R results);
}